# Vertical-center.js #


## What is this repository for? ##

Vertical center is used to center an element within its parent container. Also vertical center allows the developer to choose breakpoint for which to turn off the centering.

### To Use vertical-center.js ###

* Add jQuery and img-load.js first, then add vertical-center.js to your project.
* Add the vertical center data attribute to the element you wish to center (example: data-center="true")
* To set vertical center to turn of at specified breakpoints add a size to the data attribute instead of a boolean (example: data-center="sm" )

### Default options ###

* data-center="true" (centers element if screen width is >= 0px)
* data-center="xs" (centers element if screen width is >= 0px)
* data-center="sm" (centers element if screen width is >= 796px)
* data-center="md" (centers element if screen width is >= 992px)
* data-center="lg" (centers element if screen width is >= 1200px)
* data-center="false" (turns of vertical center)

#### Any of the default options can easily be changed by changing out the values in the switch statement inside vertical-center.js. ####

#### Dependancies ####

* jQuery
* img-load.js (used to detect if an image has fully loaded which allows vertical center to get accurate dimensions)